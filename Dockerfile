FROM debian:latest

MAINTAINER Richard Brinkman <webmaster@tsac.alpenclub.nl>

EXPOSE 9999

RUN apt-get update \
 && apt-get install -y opendmarc \
 && rm -rf /var/lib/apt/lists

CMD ["opendmarc", "-v", "-v", "-f", "-p", "inet:9999"]
